//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        cardNo:null,
        cardpwd1:null,
        cardpwd2:null,
        cardpwd3:null,
        cardpwd4:null,
        pwdInputLen: 4,
        userInfo:{},
        hasUserInfo:false
    },
    onLoad: function (options) {
        this.setData({cardNo:null});
        this.setData({cardpwd1:null});
        this.setData({cardpwd2:null});
        this.setData({cardpwd3:null});
        this.setData({cardpwd4:null});
        wx.getUserInfo({
            success: res => {
              //如果用户点击过授权，可以直接获取到信息
              console.log(res.userInfo)
            },
            fail: err => {
              //如果用户没有点击过授权，或者清除缓存，删除小程序，重新进入，会进入这里
              //然后你就可以进行你的操作，弹窗或者跳页面
              //跳页面推荐使用重定向 wx.redirectTo({}) 
              console.log(err.errMsg)
            }
          })
    },
    clearAll() {
        this.setData({
            cardNo:null,
            cardpwd1:null,
            cardpwd2:null,
            cardpwd3:null,
            cardpwd4:null
        })
    },
    bindCardNo(e) {
        this.setData({
            cardNo: e.detail.value,
        })
    },
    bindCardPwd1(e) {
        this.setData({
            cardpwd1: e.detail.value
        })
    },
    bindCardPwd2(e) {
        this.setData({
            cardpwd2: e.detail.value
        })
    },
    bindCardPwd3(e) {
        this.setData({
            cardpwd3: e.detail.value
        })
    },
    bindCardPwd4(e) {
        this.setData({
            cardpwd4: e.detail.value
        })
    },
    confrim() {
        var cardNo = this.data.cardNo;
        if(cardNo == null) {
            wx.showToast({
                title: '请输入卡号',
                icon: 'none',
                duration: 2000,
                mask: true
            })
            return;
        }
        var cardpwd1 = this.data.cardpwd1;
        var cardpwd2 = this.data.cardpwd2;
        var cardpwd3 = this.data.cardpwd3;
        var cardpwd4 = this.data.cardpwd4;
        if(cardpwd1 == null || cardpwd2 == null || cardpwd3 == null || cardpwd4 == null) {
            wx.showToast({
                title: '请输入正确的密码',
                icon: 'none',
                duration: 2000,
                mask: true
            })
            return;
        }
        var cardPwd = cardpwd1.toUpperCase() + '-' + cardpwd2.toUpperCase()
                      + '-' + cardpwd3.toUpperCase() + '-' + cardpwd4.toUpperCase();
        
        const db = wx.cloud.database();
        db.collection('th_cards').where({
            cardcode: cardNo,
            thcode: cardPwd
        }).get({
            success: function(res) {
                if(res.data.length == 0) {
                    wx.showToast({
                        title: '卡号与密码不匹配，请重新输入',
                        icon: 'none',
                        duration: 2000,
                        mask: true
                    })
                    return;
                } else {
                    if(res.data[0].status == 0) {
                        wx.showToast({
                            title: '该卡已失效',
                            icon: 'none',
                            duration: 2000,
                            mask: true
                        })
                        return;
                    }
                    wx.navigateTo({
                        url: '/pages/pickup/pickup?p=' + JSON.stringify({cardNo:"cardNo",cardPwd:"cardPwd"}),
                    })
                }
            }
        })
    }
})