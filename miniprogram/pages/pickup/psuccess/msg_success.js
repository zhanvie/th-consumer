// pages/pickup/psuccess/msg_success.js
const app = getApp()

Page({
    mixins: [require('../../mixin/themeChanged')],
    data:{
        info: {
            thdate: null,
            OMS: null
        }
    },
    onLoad: function(options) {
        // 获取发货日期和订单号
        var param = JSON.parse(options.p);
        this.setData({
            info: {
                thdate: param.thdate,
                OMS: param.OMS
            }
        });
    },
    goback() {
        wx.switchTab({
          url: '/pages/index/index',
          success: function(e) {
                var page = getCurrentPages().pop();
                if (page == undefined || page == null) return
                page.onLoad();
            }
        })
    }
});